|**Workshop**|**Protein bioinformatics for beginners**|
|----------|:-------------:|------:|
|**Dates**|8 - 9 November|
|**Time**|09:30 - 17:00 hrs|
|**Venue**|ATC Computer lab, EMBL Heidelberg|
|**Trainers**|Toby Gibson, Marc Gouw, Michael Kuhn, Manjeet Kumar, Benjamin Lang, Malvika Sharan|

## List of resources that will be covered in this workshop

**Part-1 Protein databases and sequence analysis**

1. Protein databases:
    - [Introduction to protein databases](https://git.embl.de/sharan/protein-bioinformatics-nov-2016/blob/master/TeachingMaterials/Protein_session-1.pdf) - Malvika
    - [Quick overview of NCBI](https://git.embl.de/sharan/protein-bioinformatics-nov-2016/blob/master/TeachingMaterials/protein_database.md) - Malvika
    - UniProt - Ben
        - Swissprot and Trembl 
        - Cross-refrences and link-outs
            - OMIM, Domains, GO, ...
2. Study of similar sequences - Marc
    - BLAST
        - BLASTp, BLASTn, PSI-BLAST, ...
    - Diamond
    - HMMER  
    - HHPred
3. [Multiple sequence alignments](https://git.embl.de/sharan/protein-bioinformatics-nov-2016/blob/master/TeachingMaterials/multiple_sequence_alignment.md) - Malvika
    - Clustal omega (EMBL-EBI)
    - COBALT (NCBI)
4. Other resources
    - [Human Protein Atlas](https://git.embl.de/sharan/protein-bioinformatics-nov-2016/blob/master/TeachingMaterials/HPRexercise.md) - Toby
    - [Antibodypedia](https://git.embl.de/sharan/protein-bioinformatics-nov-2016/blob/master/TeachingMaterials/Antibodypedia.md) - Toby
    - [EMBOSS toolkits](https://git.embl.de/sharan/protein-bioinformatics-nov-2016/blob/master/TeachingMaterials/EMBOSS_EBI.md) - Malvika
        - EMBOSS dot-plot
        - dotmatcher
        - Pepinfo
        - Sixpack

**Part-2 Protein structure analysis**

*Lecture (Toby):* Secondary vs tertiary structure vs protein complexes

1. Protein Structures: Toby
    - Structure database: PDB
    - Structure visualization
        - Chimera
2. Structure prediction: Malvika
    - jpred: Jalview
    - Tertiary structure prediction: Phyre2, I-TASSER
3. [Domain databases](https://docs.google.com/document/d/1v7JM9i7yANHasTdpLFZIx_oKIx5K-t0S2o5GnDbXKD0/edit): Manjeet
    - InterPro & InterProScan
    - [SMART](http://smart.embl-heidelberg.de/)
    - [Pfam](http://pfam.xfam.org/)
    - [Superfamily](http://supfam.org/SUPERFAMILY/) 
4. [Prediction of transmembrane helices in proteins](https://docs.google.com/document/d/1v7JM9i7yANHasTdpLFZIx_oKIx5K-t0S2o5GnDbXKD0/edit): Manjeet
    - [TMHMM](http://www.cbs.dtu.dk/services/TMHMM/)
    - [IUPRED](http://iupred.enzim.hu/) and [Anchor](http://anchor.enzim.hu/)
5. Intrinsically disordered region: Marc
    - ELM
    - DISPROT
6. Protein-protein interaction: Michael
    - STRING and STITCH
    - Intact
    - MINT
7. Motif visualization:
     - Weblogo (Malvika)
     - MEME (Malvika)
     - Jalview (Toby)

### Important references
    - http://www.sciencedirect.com/science/book/9788131222973
    - http://molbiol-tools.ca/Protein_Chemistry.htm
    - http://www.ebi.ac.uk/Tools/pfa/

### [Post workshop survey](https://www.surveymonkey.de/r/2GCN32Q)